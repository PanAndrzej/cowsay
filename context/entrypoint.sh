#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay -f turtle
else
   /usr/games/cowsay -f turtle "$@"
fi
